<?php

namespace Officient\OrmEssentials;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class OrmEssentialsBundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\OrmEssentials
 */
class OrmEssentialsBundle extends Bundle
{

}