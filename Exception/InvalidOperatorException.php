<?php

namespace Officient\OrmEssentials\Exception;

/**
 * Class CriterionException
 * @package Officient\OrmEssentials\Exception
 */
class InvalidOperatorException extends \Exception
{
    //
}