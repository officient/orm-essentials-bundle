<?php

namespace Officient\OrmEssentials;

use Doctrine\ORM\QueryBuilder;
use Officient\OrmEssentials\Exception\InvalidOperatorException;

/**
 * Class Criterion
 * @package Officient\OrmEssentials
 */
class Criterion implements \JsonSerializable
{
    public const OPERATORS = [
        '=',
        '!=',
        '<',
        '!<',
        '<=',
        '>',
        '!>',
        '>=',
        '<>',
        'LIKE',
        'NOT LIKE',
        'IN',
        'NOT IN',
        'IS NULL',
        'IS NOT NULL',
        'BETWEEN',
        'NOT BETWEEN',
        'IS EMPTY',
        'IS NOT EMPTY',
        'MOD'
    ];

    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $operator;

    /**
     * @var null
     */
    private $value1;

    /**
     * @var null
     */
    private $value2;

    /**
     * Criterion constructor.
     * @param string $field
     * @param string $operator
     * @param null $value1
     * @param null $value2
     * @throws \Exception
     */
    public function __construct(string $field, string $operator, $value1 = null, $value2 = null)
    {
        $operator = strtoupper($operator);

        if(in_array($operator, self::OPERATORS) === false) {
            throw new InvalidOperatorException("Operators can be one of ".print_r(self::OPERATORS, true));
        }
        $this->field = $field;
        $this->operator = $operator;
        $this->value1 = $value1;
        $this->value2 = $value2;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getField(): string
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return Criterion
     */
    public function setField(string $field): Criterion
    {
        $this->field = $field;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     * @return Criterion
     */
    public function setOperator(string $operator): Criterion
    {
        $this->operator = $operator;
        return $this;
    }

    /**
     * @return null
     */
    public function getValue1()
    {
        return $this->value1;
    }

    /**
     * @param null $value1
     * @return Criterion
     */
    public function setValue1($value1)
    {
        $this->value1 = $value1;
        return $this;
    }

    /**
     * @return null
     */
    public function getValue2()
    {
        return $this->value2;
    }

    /**
     * @param null $value2
     * @return Criterion
     */
    public function setValue2($value2)
    {
        $this->value2 = $value2;
        return $this;
    }

    /**
     * @param QueryBuilder $builder
     */
    public function andWhere(QueryBuilder $builder)
    {
        $alias = $builder->getAllAliases()[0];
        $field = $alias.".".$this->field;
        $value1Name = $this->field."_".$this->generateRandomString()."_value1";
        $value2Name = $this->field."_".$this->generateRandomString()."_value2";
        switch($this->operator) {
            case 'IS NULL':
                $builder->andWhere($builder->expr()->isNull($field));
                break;
            case 'IS NOT NULL':
                $builder->andWhere($builder->expr()->isNotNull($field));
                break;
            case 'IN':
                if(is_string($this->value1)) {
                    $this->value1 = $this->stringToArray($this->value1);
                }
                $builder->andWhere($builder->expr()->in($field, ":{$value1Name}"));
                $builder->setParameter($value1Name, $this->value1);
                break;
            case 'NOT IN':
                if(is_string($this->value1)) {
                    $this->value1 = $this->stringToArray($this->value1);
                }
                $builder->andWhere($builder->expr()->notIn($field, ":{$value1Name}"));
                $builder->setParameter($value1Name, $this->value1);
                break;
            case 'BETWEEN':
                $builder->andWhere($builder->expr()->between($field, ":{$value1Name}", ":{$value2Name}"));
                $builder->setParameter($value1Name, $this->value1);
                $builder->setParameter($value2Name, $this->value2);
                break;
            case 'NOT BETWEEN':
                $builder->andWhere($builder->expr()->not($builder->expr()->between($field, ":{$value1Name}", ":{$value2Name}")));
                $builder->setParameter($value1Name, $this->value1);
                $builder->setParameter($value2Name, $this->value2);
                break;
            case 'IS EMPTY':
                $builder->andWhere($builder->expr()->orX(
                    $builder->expr()->isNull($field),
                    "TRIM({$alias}.{$this->field}) = ''"
                ));
                break;
            case 'IS NOT EMPTY':
                $builder->andWhere($builder->expr()->andX(
                    $builder->expr()->isNotNull($field),
                    "TRIM({$alias}.{$this->field}) != ''"
                ));
                break;
            case 'MOD':
                $builder->andWhere("MOD({$alias}.{$this->field}, :{$value1Name}) = :{$value2Name}");
                $builder->setParameter($value1Name, $this->value1);
                $builder->setParameter($value2Name, $this->value2);
                break;
            default:
                $builder->andWhere("{$alias}.{$this->field} {$this->operator} :{$value1Name}");
                $builder->setParameter($value1Name, $this->value1);
                break;
        }
    }

    /**
     * Converts the old way of defining arrays to the new way
     * @param string $string
     * @return array
     */
    private function stringToArray(string $string): array
    {
        $arr = explode(',', $this->value1);
        return array_map(function($a) {
            return trim($a, "'");
        }, $arr);
    }

    private function generateRandomString($length = 10): string {
       $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
       $randomString = substr(str_shuffle($characters), 0, $length);

       return $randomString;
    }
}